<?php



class Building {
	public $name;
	protected $floors;
	protected $address;

	public function __construct($name){
		$this->name = $name;
		
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}
}




class Condominium extends Building {

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

}

$building = new Building('Caswynn Building');
$condominium = new Condominium('Enzo Condo');