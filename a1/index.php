<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04 Activity</title>
</head>
<body>
	<h1>Building</h1>

	<p>The name of the building is <?= $building->name; ?>.</p>
	<?php $building->setFloors(8); ?>
	<p>The <?= $building->name; ?> has <?php echo $building->getFloors(); ?> floors.</p>
	<?php $building->setAddress('Timog Avenue, Quezon City, Philippines'); ?>
	<p>The <?= $building->name; ?> is located at <?php echo $building->getAddress(); ?>. </p>
	<?php $building->name = "Caswynn Complex"; ?>
	<p>The name of the building has been changed to <?= $building->name; ?>.</p>

	<h1>Condominium</h1>

	<p>The name of the building is <?= $condominium->name; ?>.</p>
	<?php $condominium->setFloors(5); ?>
	<p>The <?= $condominium->name; ?> has <?php echo $condominium->getFloors(); ?> floors.</p>
	<?php $condominium->setAddress('Buendia Avenue, Makati City, Philippines'); ?>
	<p>The <?= $condominium->name; ?> is located at <?php echo $condominium->getAddress(); ?>. </p>
	<?php $condominium->name = "Enzo Tower"; ?>
	<p>The name of the condominium has been changed to <?= $condominium->name; ?>.</p>
	

	
</body>
</html>
